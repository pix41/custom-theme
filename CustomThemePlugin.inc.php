<?php
// Импортируем дефолтную тему
import('plugins.themes.default.DefaultThemePlugin');

// Наследуем нашу тему от дефолтной
class CustomThemePlugin extends DefaultThemePlugin {
  // указываем 2 параметра, чтоб стили подтянулись
  public function init() {
    $this->setParent('defaultthemeplugin');
    $this->addStyle('child-stylesheet', 'styles/main.less');
    $this->modifyStyle('child-stylesheet', array('addLessVariables' => '@bg-base:' . $this->getOption('baseColour') . ';'));

    $this->addOption('darkBg', 'radio', array(
      'label' => 'plugins.themes.custom.option.darkBg.label',
      'options' => array(
        'yes' => 'plugins.themes.custom.option.darkBg.y',
        'no' => 'plugins.themes.custom.option.darkBg.n'
      ),
      'default' => 'no'
    ));
    if ($this->getOption('darkBg') == 'yes') {
      $svgStyle = '#ksu_logo #path16,#ksu_logo #path20{fill:#fff!important;stroke:#fff!important;}';
      $this->addStyle('svg-logo-style', $svgStyle, ['inline' => true]);
    }

    $request = Application::getRequest();
    $templateManager = TemplateManager::getManager($request);
    $journal = $request->getJournal();
    if (isset($journal)) {
      $locales = $journal->getSupportedLocaleNames();
      } else {
      $site = $request->getSite();
      $locales = $site->getSupportedLocaleNames();
    }
  
    if (isset($locales) && count($locales) > 1) {
      $templateManager->assign('enableLanguageToggle', true);
      $templateManager->assign('languageToggleLocales', $locales);
    }
  }

  // Указываем название темы для списка тем
  function getDisplayName() {
    return "Custom Theme";
  }

  // её описание
  function getDescription() {
    return 'Custom Theme';
  }
}